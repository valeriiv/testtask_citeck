package edu.testtask.well_formed_xml;

import edu.testtask.utils.Constants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MockWellFormedRootTag {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getRootTagBody() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(Constants.URL_ENTRIES).accept(MediaType.APPLICATION_XML)
        ).andExpect(status().is2xxSuccessful()).andExpect(content().string(equalTo(Constants.TEST_WELL_FORMED_XML_ROOT)));
    }

}
