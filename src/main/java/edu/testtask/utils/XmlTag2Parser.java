package edu.testtask.utils;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.util.List;

public class XmlTag2Parser extends BaseXmlParser {

    public String tag2Value() {

        stringBuilder = new StringBuilder();
        stringBuilder2 = new StringBuilder();
        stringBuilder3 = new StringBuilder();

        readFileToValidate(stringBuilder);

        if (!(stringBuilder.toString()).contains(Constants.XML_TAG1)) {
            saxBuilder = new SAXBuilder();

            try {
                document = saxBuilder.build(inputFile);
                Element classElement = document.getRootElement();
                List<Element> tagList = classElement.getChildren();
                Element tag;

                for (int temp = 0; temp < tagList.size(); temp++) {
                    tag = tagList.get(temp);

                    stringBuilder2.append(tag.getChild(Constants.XML_TAG2).getText());
                }

            } catch (JDOMException e) {
                e.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            return stringBuilder2.toString();

        } else {
            readFileToValidate(stringBuilder3);
            return Constants.INFO_MESSAGE;
        }
    }

}
