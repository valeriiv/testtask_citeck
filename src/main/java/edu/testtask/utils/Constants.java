package edu.testtask.utils;

public class Constants {

    public static final String PATH_TO_XML = "D:\\Folder\\IdeaProjects\\testtask\\well_formed.xml";
    public static final String TEST_WELL_FORMED_XML_ROOT = "&lt;entry&gt;&lt;field&gt;1&lt;field&gt;&lt;/entry&gt;&lt;entry&gt;&lt;field&gt;2&lt;field&gt;&lt;/entry&gt;&lt;entry&gt;&lt;field&gt;3&lt;field&gt;&lt;/entry&gt;&lt;entry&gt;&lt;field&gt;4&lt;field&gt;&lt;/entry&gt;&lt;entry&gt;&lt;field&gt;5&lt;field&gt;&lt;/entry&gt;&lt;entry&gt;&lt;field&gt;6&lt;field&gt;&lt;/entry&gt;";
    public static final String TEST_ILL_FORMED_XML_ROOT = "&lt;entry field=\"1\"/&gt;&lt;entry field=\"2\"/&gt;&lt;entry field=\"3\"/&gt;&lt;entry field=\"4\"/&gt;&lt;entry field=\"5\"/&gt;&lt;entry field=\"6\"/&gt;";
    public static final String TEST_WELL_FORMED_XML_TAG1 = "&lt;field&gt;1&lt;/field&gt;&lt;field&gt;2&lt;/field&gt;&lt;field&gt;3&lt;/field&gt;&lt;field&gt;4&lt;/field&gt;&lt;field&gt;5&lt;/field&gt;&lt;field&gt;6&lt;/field&gt;";
    public static final String TEST_WELL_FORMED_XML_TAG2 = "123456";
    public static final String XML_ROOT = "entries";
    public static final String XML_TAG = "entry";
    public static final String XML_TAG1 = "entry field=";
    public static final String XML_TAG2 = "field";
    public static final Integer XML_HEADER = 47;
    public static final Integer XML_ROOT_END = 10;
    public static final String INFO_MESSAGE = "Данный тэг не имеет тела/тело пустое";
    public static final String NO_MATCH = "Данному тэгу нет соответствия в xml-файле";
    public static final String LESS = "&lt;";
    public static final String GRATER = "&gt;";
    public static final String URL_ENTRIES = "/tagbody?name=entries";
    public static final String URL_ENTRY = "/tagbody?name=entry";
    public static final String URL_FIELD = "/tagbody?name=field";

}
