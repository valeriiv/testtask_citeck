package edu.testtask.utils;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.*;
import java.util.List;

public class XmlRootParser extends BaseXmlParser {

    public String rootTagValue() {

        stringBuilder = new StringBuilder();
        stringBuilder2 = new StringBuilder();
        stringBuilder3 = new StringBuilder();

        readFileToValidate(stringBuilder);

        if (!(stringBuilder.toString()).contains(Constants.XML_TAG1)) {
            saxBuilder = new SAXBuilder();

            try {
                document = saxBuilder.build(inputFile);
                Element classElement = document.getRootElement();
                List<Element> tagList = classElement.getChildren();
                Element tag;

                for (int temp = 0; temp < tagList.size(); temp++) {
                    tag = tagList.get(temp);

                    stringBuilder2.append(Constants.LESS + tag.getName() + Constants.GRATER);
                    stringBuilder2.append(Constants.LESS + tag.getChild(Constants.XML_TAG2).getName()
                            + Constants.GRATER);
                    stringBuilder2.append(tag.getChild(Constants.XML_TAG2).getText());
                    stringBuilder2.append(Constants.LESS + tag.getChild(Constants.XML_TAG2).getName() + Constants.GRATER);
                    stringBuilder2.append(Constants.LESS + "/" + tag.getName() + Constants.GRATER);
                }

            } catch (JDOMException e) {
                e.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            return stringBuilder2.toString();

        } else { //ill-formed
            readFileToValidate(stringBuilder3);
            String rootSubstring = stringBuilder3.substring(Constants.XML_HEADER,
                    stringBuilder3.length()-Constants.XML_ROOT_END);
            rootSubstring = rootSubstring.replaceAll("<", Constants.LESS);
            rootSubstring = rootSubstring.replaceAll(">", Constants.GRATER);
            return rootSubstring;
        }
    }

}
