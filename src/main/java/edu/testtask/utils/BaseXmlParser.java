package edu.testtask.utils;

import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;

import java.io.*;

public class BaseXmlParser {

    StringBuilder stringBuilder;
    StringBuilder stringBuilder2;
    StringBuilder stringBuilder3;
    SAXBuilder saxBuilder;
    Document document;
    File inputFile;
    BufferedReader br = null;
    String inner;

    protected void readFileToValidate(StringBuilder stringBuilder) {
        try {
          br = new BufferedReader(new FileReader(new File(Constants.PATH_TO_XML)));
            while (br.ready()) {
                inner = br.readLine();
                stringBuilder.append(inner);
            }

            inputFile = new File(Constants.PATH_TO_XML);

        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException " + e.getMessage());
        } catch (IOException e) {
            System.out.println("IOException " + e.getMessage());
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                System.out.println("IOException " + e.getMessage());
            }
        }
    }
}
