package edu.testtask.controller;

import edu.testtask.utils.Constants;
import edu.testtask.utils.XmlRootParser;
import edu.testtask.utils.XmlTag1Parser;
import edu.testtask.utils.XmlTag2Parser;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {

    XmlRootParser xmlRootParser;
    XmlTag1Parser xmlTag1Parser;
    XmlTag2Parser xmlTag2Parser;

    @RequestMapping(value = "/tagbody", method = RequestMethod.GET)
    @ResponseBody
    public String tagBody(
            @RequestParam(value = "name", name = "name") String name
    ) {
        if (name.endsWith(Constants.XML_ROOT)) {
            xmlRootParser = new XmlRootParser();
            return xmlRootParser.rootTagValue();
        }

        if (name.endsWith(Constants.XML_TAG)) {
            xmlTag1Parser = new XmlTag1Parser();
            return xmlTag1Parser.tag1Value();
        }

        if (name.endsWith(Constants.XML_TAG2)) {
            xmlTag2Parser = new XmlTag2Parser();
            return xmlTag2Parser.tag2Value();
        }
        return Constants.NO_MATCH;
    }

}
